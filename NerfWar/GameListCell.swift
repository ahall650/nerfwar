//
//  GameListCell.swift
//  NerfWar
//
//  Created by anthony hall on 3/20/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit

class GameListCell: UITableViewCell {

    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var playerNumberLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
