//
//  Game.swift
//  NerfWar
//
//  Created by anthony hall on 3/15/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit

class Game: NSObject {
    var gameName: String!
    var players: [String]?
    let ref: DatabaseReference?
    
    //init to convert database snapshot to game object
    init?(snapshot: DataSnapshot){
        self.players = []
        guard let snapshotValue = snapshot.value as? [String: Any] else { return nil }
        let gameName = snapshotValue["gameName"] as? String ?? "Unknown"
        //an array of players
        let JSONPlayers = snapshotValue["players"] as? [String : Any]
        self.gameName = gameName
        //make sure there are players in the game
        if JSONPlayers != nil {
            //add each player to game
            for playerUID in JSONPlayers!{
                self.players?.append(playerUID.key)
            }
        }

        self.ref = snapshot.ref
        
    }
    //create a game with players
    init(gameName: String, players: [String]){
        self.gameName = gameName
        self.players = players
        self.ref =  nil
    }
    //create a game with no players
    init(gameName: String){
        self.gameName = gameName
        self.players = nil
        self.ref = nil
    }
    //convert game object to json for posting to database
    func toAnyObject() -> Any{
  
        return[
            "gameName" : gameName,
            "players" : players!
        ]
    }
}
