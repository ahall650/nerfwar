//
//  Player.swift
//  NerfWar
//
//  Created by anthony hall on 3/2/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

//import Foundation
import UIKit

class Player: NSObject {
    var uid: String
    var email: String
    var username: String!
    var currentSelected: Bool
    var isOnline: Bool
    let ref: DatabaseReference?
    
    //init for converting a snapshot to a player object
    init?(snapshot: DataSnapshot) {
        guard let snapshotValue = snapshot.value as? [String: Any] else { return nil }
        guard let uid = snapshotValue["uid"] as? String else { return nil }
        guard let email = snapshotValue["email"] as? String else { return nil }
        guard let username = snapshotValue["username"] as? String else { return nil }
        guard let currentSelected = snapshotValue["currentSelected"] as? Bool else { return nil }
        guard let isOnline = snapshotValue["isOnline"] as? Bool else { return nil }

        self.uid = uid
        self.email = email
        self.username = username
        self.currentSelected = currentSelected
        self.isOnline = isOnline

        self.ref = snapshot.ref
        
    }
    //
    //not used yet
    init?(jsonDict: [String : Any]){
        guard let uid = jsonDict["uid"] as? String else { return nil }
        guard let email = jsonDict["email"] as? String else { return nil }
        guard let username = jsonDict["username"] as? String else { return nil }
        guard let currentSelected = jsonDict["currentSelected"] as? Bool else { return nil }
        guard let isOnline = jsonDict["isOnline"] as? Bool else { return nil }

        self.uid = uid
        self.email = email
        self.username = username
        self.currentSelected = currentSelected
        self.isOnline = isOnline
        self.ref = nil
    
    }
//not used yet
    class func convertJSONPlayersToObject(jsonDict: [String : Any ]) -> [Player]{
        var playerObjects = [Player]()
        let playersRef = Database.database().reference(withPath: "players")

        for currentPlayer in jsonDict {
            //let newPlayer = Player(jsonDict: currentPlayer)
            //playerObjects.append(newPlayer!)
            playersRef.child(currentPlayer.key).observeSingleEvent(of: .value, with: { (snapshot) in
                let newPlayer = Player(snapshot: snapshot)
                playerObjects.append(newPlayer!)
            })

        }
        return playerObjects
    }
    //standard init used to create object for first time
    init(uid: String, email: String, name:String) {
        self.uid = uid
        self.email = email
        self.username = name
        self.currentSelected = false
        self.isOnline = false
        self.ref = nil
    }
    //convert player object to json for posting to database
    func toAnyObject() -> Any{
        return[
            "uid" : uid,
            "email" : email,
            "username" : username,
            "currentSelected" : currentSelected,
            "isOnline" : isOnline
        ]
    }

}
