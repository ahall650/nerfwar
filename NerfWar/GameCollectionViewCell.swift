//
//  GameCollectionViewCell.swift
//  NerfWar
//
//  Created by anthony hall on 3/29/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit

class GameCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var playerEmailLabel: UILabel!
    @IBOutlet weak var hitCountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
