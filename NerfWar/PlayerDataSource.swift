//
//  PlayerDataSource.swift
//  NerfWar
//
//  Created by anthony hall on 3/11/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

//THIS ISN'T BEING USED

import UIKit
import FirebaseDatabaseUI
class PlayerDataSource: FUITableViewDataSource {
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            snapshot(at: indexPath.row).ref.removeValue()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.count != 0 {
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        return Int(self.count)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
}

