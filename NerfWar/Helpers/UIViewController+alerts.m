//
//  UIViewController+alerts.m
//  NerfWar
//
//  Created by anthony hall on 3/3/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//


#import "UIViewController+alerts.h"

@implementation UIViewController (alerts)
- (void)showNewUserPromptWithMessage:(NSString *)message completionBlock:(void(^)(BOOL userPressedOK, NSString* username, NSString* email, NSString* password))completion {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"New Account:"
                                                                              message: message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Username";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"password";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray *textfields = alertController.textFields;
        UITextField *nameField =[textfields objectAtIndex:0];
        NSString *username = nameField.text;
        UITextField *emailField =[textfields objectAtIndex:1];
        NSString *email = emailField.text;
        UITextField *passwordField =[textfields objectAtIndex:2];
        NSString *password = passwordField.text;
        completion(YES, username, email, password);

        //TODO check for proper values
        /*
        if ([namefield.text isEqualToString:@""]) {
            [self presentOwnerEntryViewWithMesage:@"Please Enter a Name"];
        }else{
            [self completeCheckoutWithOwner:namefield.text];
            
        }
         */
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)showNewGamePromptWithMessage:(NSString *)message completionBlock:(void(^)(BOOL userPressedOK, NSString* gameName))completion {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"New Game:"
                                                                              message: message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"gameName";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray *textfields = alertController.textFields;
        UITextField *nameField =[textfields objectAtIndex:0];
        NSString *gameName = nameField.text;

        completion(YES, gameName);
        
        //TODO check for proper values
        /*
         if ([namefield.text isEqualToString:@""]) {
         [self presentOwnerEntryViewWithMesage:@"Please Enter a Name"];
         }else{
         [self completeCheckoutWithOwner:namefield.text];
         
         }
         */
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];


}
@end
