//
//  UIViewController+alerts.h
//  NerfWar
//
//  Created by anthony hall on 3/3/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^AlertPromptCompletionBlock)(BOOL userPressedOK, NSString* userInput);

@interface UIViewController (alerts)
- (void)showNewUserPromptWithMessage:(NSString *)message completionBlock:(void(^)(BOOL userPressedOK, NSString* username, NSString* email, NSString* password))completion;

-(void)showNewGamePromptWithMessage:(NSString *)message completionBlock:(void(^)(BOOL userPressedOK, NSString* gameName))completion;
@end
