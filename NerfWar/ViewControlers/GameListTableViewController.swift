//
//  GameListTableViewController.swift
//  NerfWar
//
//  Created by anthony hall on 3/2/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI

class GameListTableViewController: UITableViewController {
    let toPlayerListController = "toPlayerListController"
    let toGameInfoController = "toGameInfoController"
    var ref: DatabaseReference!
    var dataSource: FUITableViewDataSource?


    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        let nib = UINib(nibName: "GameListCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "GameListCell")
        
        dataSource = FUITableViewDataSource(query: getQuery()) {(tableView, indexPath, snap) -> UITableViewCell in
            let cell: UITableViewCell
            
            cell = self.tableView.dequeueReusableCell(withIdentifier: "GameListCell", for: indexPath)
            let gameCell = cell as? GameListCell
            //convert database snapshot to game object
            let game = Game(snapshot: snap)
            gameCell?.gameNameLabel.text = game?.gameName ?? ""
            gameCell?.playerNumberLabel.text = "\(game?.players?.count ?? 0) Players"

            return cell
            
        }
        dataSource?.bind(to: self.tableView)


    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
        ref.removeAllObservers()
    }
    @IBAction func addGamePressed(_ sender: Any) {

        self.showNewGamePrompt(withMessage: "Add New Game", completionBlock: {(userPressedSave, gameName) in
            let gameRef = self.ref.child("games").child(gameName!)
            gameRef.setValue(["gameName" : gameName!])

            })
 
    }
    func getQuery() -> DatabaseQuery{
        let query = (ref.child("games")).queryOrdered(byChild: "gameName")
        return query

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == self.toGameInfoController {
            let controller = segue.destination as! GameInfoViewController
            let indexPath = sender as! IndexPath
            let snap = dataSource?.snapshot(at: indexPath.row)
            let game = snap!.value as! NSDictionary

            //set the gameViewConroller properties with the selected game values
            controller.gameKey = game["gameName"] as! String
            controller.userKey = (Auth.auth().currentUser?.uid)




        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: self.toGameInfoController, sender: indexPath)

    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
  
        return 60
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }



}
