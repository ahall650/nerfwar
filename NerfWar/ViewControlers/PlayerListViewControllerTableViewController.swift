//
//  PlayerListViewControllerTableViewController.swift
//  NerfWar
//
//  Created by anthony hall on 3/11/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI

/////this class isn't currently being used
//// list all players that are online
class PlayerListTableViewController: UITableViewController {
    var ref: DatabaseReference!
    var dataSource: FUITableViewDataSource?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()

        dataSource = FUITableViewDataSource(query: getQuery()) {(tableView, indexPath, snap) -> UITableViewCell in
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)
            guard let player = Player(snapshot: snap) else { return cell }
            if player.isOnline{
                cell.textLabel?.textColor = UIColor.green
                cell.detailTextLabel?.text = "Online"
            }else{
                cell.detailTextLabel?.text = "Offline"
            }
            cell.textLabel?.text = player.username
            
            return cell
        }
        dataSource?.bind(to: self.tableView)
        

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
        ref.removeAllObservers()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func getUid() -> String {
        return (Auth.auth().currentUser?.uid)!
    }
    
    func getQuery() -> DatabaseQuery {
        let query = (ref.child("players")).queryOrdered(byChild: "isOnline").queryEqual(toValue: true)

        return query
    }
    
    @IBAction func logOut(_ sender: Any){
        let playersRef = Database.database().reference(withPath: "players")
        
        let userID = (Auth.auth().currentUser?.uid)
        let playerRefObj = playersRef.child(userID!)
        playerRefObj.updateChildValues(["isOnline" : false])
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch {
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
