//
//  GameViewController.swift
//  NerfWar
//
//  Created by anthony hall on 3/29/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit
import FirebaseDatabaseUI

private let reuseIdentifier = "GameCollectionViewCell"

class GameViewController: UICollectionViewController {
    var ref: DatabaseReference!
    var dataSource: FUICollectionViewDataSource?
    var currentGame: Game!
    var myUID: String!
    var myIndexPath: IndexPath!
    var gamePlayerRef: DatabaseReference!

    override func viewDidLoad() {

        super.viewDidLoad()
        ref = Database.database().reference()
        myUID = (Auth.auth().currentUser?.uid)!
        //reference to my player in game
        gamePlayerRef = ref.child("games").child(self.currentGame.gameName).child("players").child(myUID)

        //register custom cell
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        self.collectionView?.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
        
        //main section for loading cell info will update on change in database
        dataSource = FUICollectionViewDataSource(query: getQuery(), populateCell: {(collectionView, indexPath, snap) -> UICollectionViewCell in
            let cell: UICollectionViewCell
            cell = self.collectionView!.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
            let gameCell = cell as? GameCollectionViewCell
           // print(snap.value)
            //convert snapshot to dictionary
            let dic = snap.value as! [String : Any]
            gameCell?.hitCountLabel.text = "\(dic["hitCount"] ?? "EMPTY")"

            //get my player from the player database
            self.ref.child("players").child(snap.key).observeSingleEvent(of: .value, with: {(snapshot) in
                //convert dictionary to player object
                let player = Player(snapshot: snapshot)
                gameCell?.playerNameLabel.text = player?.username
                gameCell?.playerEmailLabel.text = player?.email
                

            })
            return cell

            })
        dataSource?.bind(to: collectionView!)
    }
    
    
    func getQuery() -> DatabaseQuery{
        let query = (ref.child("games").child(currentGame.gameName).child("players"))
        return query
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //get the UID of the player tapped
        //let snap = dataSource?.snapshot(at: indexPath.row)
        //TODO add the player UID to the data about the kill
        
        //get the current hit count and add 1 to it
        gamePlayerRef.observeSingleEvent(of: .value, with: {(snapshot) in
            let dic = snapshot.value as! [String : Any]
            let currentHitCount = dic["hitCount"] as! Int
            self.gamePlayerRef.child("hitCount").setValue(currentHitCount + 1)
        })
        
        

    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }



}
