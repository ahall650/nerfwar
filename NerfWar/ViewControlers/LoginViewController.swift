//
//  LoginViewController.swift
//  NerfWar
//
//  Created by anthony hall on 3/2/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    let toGameListController = "toGameListController"
    var ref: DatabaseReference!
    

    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!

    override func viewWillAppear(_ animated: Bool) {
        Auth.auth().addStateDidChangeListener { auth, user in
            //if user is already logged in skip the sign in process
            if user != nil{
                print(user!.uid)
                self.setUserOnlineWith(userID: user!.uid)
                //when auth state changes go to next screen
                self.performSegue(withIdentifier: self.toGameListController, sender: nil)

                }
            }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()

        }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        ref.removeAllObservers()
    }

    @IBAction func signUpPressed(_ sender: Any) {
        //show a UIAlertViewController for signup
        self.showNewUserPrompt(withMessage: "Sign Up", completionBlock: {(userPressedSave, username, email, password) in
            
            //create new user with user provided username, email and password
            Auth.auth().createUser(withEmail: email!, password: password!, completion: { user, error in
                if error == nil {
                    //if no error, user was creted now login
                    let playersRef = Database.database().reference(withPath: "players")

                    let player = Player(uid: user!.uid, email: user!.email!, name: username!)
                    let playerRefObj = playersRef.child(user!.uid)
                    playerRefObj.setValue(player.toAnyObject())
                    Auth.auth().signIn(withEmail: email!, password: password!, completion: {user, error in
                        if error != nil{
                            //TODO PRESENT ERROR MESSAGE
                            print("error logging in %@", error! )
                        }
                    })

                }else{
                    print(error ?? "NO CREATE ERROR")
                }
            })
        })

    }

    @IBAction func loginPressed(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!, completion: {user, error in
            if error != nil{
                //TODO PRESENT ERROR MESSAGE
                print("error logging in %@", error! )
            }
            
        })
    }
    //set user property online
    //TODO inplement an offline function
    //not really used yet but should be queried for not including in game
    func setUserOnlineWith(userID: String)->Void{
        let playersRef = Database.database().reference(withPath: "players")
        let playerRefObj = playersRef.child(userID)
        let online = true
        playerRefObj.updateChildValues(["isOnline" : online])
    }
    
    // MARK: - Navigation
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
