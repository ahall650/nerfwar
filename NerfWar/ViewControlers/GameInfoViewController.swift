//
//  GameInfoViewController.swift
//  NerfWar
//
//  Created by anthony hall on 3/21/18.
//  Copyright © 2018 anthony hall. All rights reserved.
//

import UIKit

class GameInfoViewController: UIViewController {
    var gameKey: String!
    var userKey: String!
    var playerNumbers: Int?
    var currentGame: Game!
    var currentPlayer: Player!
    var ref: DatabaseReference!
    let toGameViewController = "toGameViewController"

    @IBOutlet weak var playerNumberLabel: UILabel!
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var joinGameButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        let gameRef = ref.child("games").child(self.gameKey)
        let userRef = ref.child("players").child(userKey)
        gameRef.observe(.value, with: { (snapshot) in
            self.currentGame = Game(snapshot: snapshot)
            self.gameNameLabel.text? =  self.currentGame.gameName
            self.playerNumberLabel.text = "\(self.currentGame.players?.count ?? 0) Players"
        })
        userRef.observe(.value, with: {(snapshot) in
            self.currentPlayer = Player(snapshot: snapshot)
            self.userNameLabel.text? = self.currentPlayer.username

        })



    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == self.toGameViewController {
            let controller = segue.destination as! GameViewController
            controller.currentGame = self.currentGame
   
        }
    }

    @IBAction func joinGame(_ sender: Any) {
        //get reference to game
        let gameRef = ref.child("games").child(self.gameKey)
        //reference to players
        let participants = gameRef.child("players")
        //start player with a 0 hit count
        participants.child(self.currentPlayer.uid).setValue(["hitCount":0])
        //add a property to player object that records that the player is part of this game.
        //TODO when payer leaves game make a new array with old games to seperate from current games
        ref.child("players").child(self.currentPlayer.uid).child("games").child(self.gameKey).setValue("true")
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
